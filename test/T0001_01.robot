***Settings***
Resource    common/settings.robot
Resource    common/variables.robot
Resource    common/keywords.robot


***Test Cases***
Get_clinics
    [Tags]  clinic  get
    ${response}   Call_Api_get_the_response  ${GET_CLINICS}   null
    ${status}=      convert to string   ${response.status_code}
    should be equal     ${status}     200

Get_Medical_Services
    [Tags]  clinic  get
    ${payload}=     Get_payload_details     medical_services
    ${response}   Call_Api_get_the_response  ${GET_MEDICAL_SERVICES}  ${payload}
    ${status}=      convert to string   ${response.status_code}
    should be equal     ${status}     200
