***Keywords***
Call_Api_get_the_response
    [Arguments]     ${api_name}     ${payload}
    Log     ${payload}
    ${BASE_URL}=        Get_attribute_from_config   appUrl
    create session      Call_Api      ${BASE_URL}   
    ${SUBSCRIPTION_KEY}=    Get_attribute_from_config   subscriptionKey
    &{HEADERS}=     create dictionary   Ocp-Apim-Subscription-Key=${SUBSCRIPTION_KEY}  content-type=application/json 
    ${response}=    post request     Call_Api    ${api_name}     data=${payload}    headers=${HEADERS}
    ${status_code}=     convert to string   ${response.status_code}
    Log     ${status_code}
    Log     ${response.json()}
    [Return]        ${response}

Get_attribute_from_config
    [Arguments]     ${param}
    ${CONFIG}=      Run Keyword If      "${CONFIG}"=="null"     Load_Env_Config        
    [Return]        ${CONFIG['${param}']}

Load_Env_Config
    ${environment_file}=    get file    resource/config/environment.json
    ${environment_json}=     evaluate    ${environment_file} 
    [Return]        ${environment_json['${ENV}']}

Get_payload_details
    [Arguments]     ${api_name}
    ${json}=    get file    ${PAYLOAD_PATH}/${api_name}.json
    ${payload}=     catenate    ${json} 
    [Return]    ${payload}
