FROM python:3

RUN python3 -m pip install robotframework

RUN python3 -m pip install robotframework-requests

RUN python3 -m pip install robotframework-extendedrequestslibrary

ENTRYPOINT ["/run_tests.sh"]