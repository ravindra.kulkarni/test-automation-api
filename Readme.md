``API Automation`` is a automation frmaework for acceptance tests.

# Prerequisits

Python 3.X - https://www.python.org/downloads/
Robot Framework - https://robotframework.org/

Install python into your local system and few of the libraries via ``pip``. Please find below instructions for the same.

```bash
pip install -U requests
pip install -U robotframework-requests
```

# Setup

Use any of the bash interfaces like [Git Bash](https://git-scm.com/downloads); this is just for the interface

If you have set up any proxy in your local like [CTNLM](http://cntlm.sourceforge.net/) then configure the same for the enironment as below.

```bash
export http_proxy=http://localhost:3128 #configure the port you are running
export https_proxy=http://localhost:3128
```

# How to run the application

* Open the git bash console or terminal for checkedout folder.
* Run the followng command.

```bash
./run_tests.sh
```